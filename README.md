# Lynx theme

Lynx is a theme for GNOME/GTK based desktop environments. Based on Orchis

## Requirements

- GTK `>=3.20`
- `gnome-themes-extra` (or `gnome-themes-standard`)
- Murrine engine — The package name depends on the distro.
  - `gtk-engine-murrine` on Arch Linux
  - `gtk-murrine-engine` on Fedora
  - `gtk2-engine-murrine` on openSUSE
  - `gtk2-engines-murrine` on Debian, Ubuntu, etc.
- `sassc` — build dependency

## Installation

### Manual Installation

Run the following commands in the terminal:

```sh
./install.sh
```

> Tip: `./install.sh` allows the following options:

```
OPTIONS:
  -d, --dest DIR          Specify destination directory (Default: /home/pato/.themes)
  -n, --name NAME         Specify theme name (Default: Lynx)

  -t, --theme VARIANT     Specify theme color variant(s) [default|purple|alt|all]
  -c, --color VARIANT     Specify color variant(s) [standard|light|dark] (Default: All variants)s)
  -s, --size VARIANT      Specify size variant [standard|compact] (Default: All variants)

  -l, --libadwaita        Link installed Lynx gtk-4.0 theme to config folder for all libadwaita app use Lynx theme

  -r, --remove,
  -u, --uninstall         Uninstall/Remove installed themes

  --tweaks                Specify versions for tweaks [solid|compact|black] (Options can mix)
                          1. solid:    no transparency panel variant
                          2. compact:  no floating panel variant
                          3. black:    full black variant(Default is Green)

  --shell                 install gnome-shell version [38|40|42]
                          1. 38:       gnome-shell version < 40.0
                          2. 40:       gnome-shell version = 40.0
                          3. 42:       gnome-shell version = 42.0

  -h, --help              Show help
```

> For more information, run: `./install.sh -h`


### Flatpak Installation

Automatically install your host GTK+ 3.0 theme as a Flatpak. Use this:

- [stylepak](https://github.com/refi64/stylepak)

Also if you want to use this theme on a GTK+ 4.0 flatpak app, you can give the permission to access this file

local:
```
flatpak override --user --filesystem=xdg-config/gtk-4.0
```

global:
```
flatpak override --filesystem=xdg-config/gtk-4.0
```

### Firefox theme
[Install Firefox theme](src/firefox)

### Fix for Dash to panel
Go to `src/gnome-shell/extensions/dash-to-panel` [dash-to-panel](src/gnome-shell/extensions/dash-to-panel) run the following commands in the terminal:

```sh
./install.sh
```
